from flask import Flask, render_template, request

app = Flask(__name__)


@app.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "POST":
        datei = request.form.to_dict().get("user")
        return render_template("login.html", name=datei)
    return render_template("login.html")


if __name__ == "__main__":
    app.run(debug=True)
